﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartBar
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.Shown += MainForm_Shown;
            this.lblBarStatus.Text = "Status: ";
        }

        void MainForm_Shown(object sender, EventArgs e)
        {
            SmartBar.Bar.CurrentBar.LoadSettings();
            Bar.CurrentBar.InitializeDBConnection();
            SmartBar.Bar.CurrentBar.InitializePort();
            Bar.CurrentBar.LoadDataDrinks();
            Bar.CurrentBar.LoadDataCocktails();
            Bar.CurrentBar.LoadCocktailIngredients();
            Bar.CurrentBar.ReadyStatusChanged += CurrentBar_ReadyStatusChanged;
            PopulateDrinkPanel();
        }

        void PopulateDrinkPanel()
        {
            this.pnlVolumeUCs.Controls.Clear();
            int x = 0;
            int y = 0;
            foreach (Drink d in Bar.CurrentBar.AvailableDrinks)
            {
                DrinkVolumeUC uc = new DrinkVolumeUC();
                uc.SetDrink(d);
                uc.Size = new Size(uc.Width, pnlVolumeUCs.Height - 25);
                uc.Location = new Point(x, y);
                x += uc.Width;
                pnlVolumeUCs.Controls.Add(uc);
            }
        }

        void CurrentBar_ReadyStatusChanged(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(() => { CurrentBar_ReadyStatusChanged(sender, e);}));
                return;
            }
            this.lblBarStatus.Text = string.Format("Status: {0}", Bar.CurrentBar.BarReady ? "Gereed" : "Bezig");
        }

        private void btnCustomDrank_Click(object sender, EventArgs e)
        {
            CocktailForm form = new CocktailForm();
            form.Show();
            this.Visible = false;
            form.FormClosed += otherFormClosed;
        }

        void otherFormClosed(object sender, FormClosedEventArgs e)
        {
            this.Visible = true;
            PopulateDrinkPanel();
        }

        private void btnPresetDrank_Click(object sender, EventArgs e)
        {
            CocktailListForm form = new CocktailListForm();
            form.Show();
            this.Visible = false;
            form.FormClosed += otherFormClosed;
        }
    }
}
