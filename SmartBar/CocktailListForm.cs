﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartBar
{
    public partial class CocktailListForm : Form
    {
        public CocktailListForm()
        {
            InitializeComponent();
            this.Shown += CocktailListForm_Shown;
        }

        void CocktailListForm_Shown(object sender, EventArgs e)
        {
            int x = 0;
            int y = 0;
            Dictionary<Cocktail, int> orderedCocktails = Bar.CurrentBar.GetCocktailOrdersDesc();
            for (int i = 0; i < orderedCocktails.Count; i++)
            {
                KeyValuePair<Cocktail, int> kvp = orderedCocktails.ElementAt(i);
                Cocktail cocktail = kvp.Key;
                if (!cocktail.CanPour())
                    continue;
                CocktailUC uc = new CocktailUC();
                uc.SetCocktail(cocktail, kvp.Value);
                uc.Location = new Point(x, y);
                uc.Clicked += uc_Clicked;
                y += uc.Height;
                this.Controls.Add(uc);
            }
        }

        void uc_Clicked(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
