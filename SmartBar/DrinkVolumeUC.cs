﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartBar
{
    public partial class DrinkVolumeUC : UserControl
    {
        Drink drink;

        public DrinkVolumeUC()
        {
            InitializeComponent();
        }

        public void SetDrink(Drink drink)
        {
            this.drink = drink;
            this.pcbDrink.Image = drink.Image.Image;
            this.pgbVolume.Maximum = drink.MaxVolume;
            this.pgbVolume.Value = drink.CurrentVolume;
            this.lblVolume.Text = string.Format("{0}/{1} ml.", drink.CurrentVolume, drink.MaxVolume);
        }

        private void pgbVolume_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bijvullen?", "Bijvullen", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                drink.CurrentVolume = drink.MaxVolume;
                Bar.CurrentBar.UpdateDrinkVolume(drink);
                this.pgbVolume.Value = drink.CurrentVolume;
                this.lblVolume.Text = string.Format("{0}/{1} ml.", drink.CurrentVolume, drink.MaxVolume);
            }
        }
    }
}
