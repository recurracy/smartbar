﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SmartBar
{
    public class Drink
    {
        /// <summary>
        /// Volume of pouring resevoir in ml. One serving is equal to the value of this field.
        /// </summary>
        public const int ResevoirVolume = 50; // zodra maatschenkers beschikbaar zijn, dit toewijzen.
        /// <summary>
        /// The name of this drink
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// The maximum volume in ml a bottle can contain, as determined from the netto ml on the etiquette of the bottle.
        /// </summary>
        public int MaxVolume { get; private set; }
        /// <summary>
        /// The current volume of fluid in ml in the bottle
        /// </summary>
        public int CurrentVolume { get; set; }
        /// <summary>
        /// The alcoholpercentage of this drink. Is displayed in whole integer numbers, not in decimals.
        /// </summary>
        public int AlcoholPerc { get; private set; }

        public ImageEx Image { get; set; }

        public Drink(string name, int maxVolume, int alcoholPerc)
        {
            this.Name = name;
            this.MaxVolume = maxVolume;
            this.AlcoholPerc = alcoholPerc;
        }

        public bool CanPour(int servings)
        {
            int totalVolume = servings * ResevoirVolume;
            return CurrentVolume >= totalVolume;
        }

        ///<summary>
        /// Determine nr of defaultglasses. One defaultglass is a unit that equals 10 grammes of alcohol.
        ///</summary>
        public double GetNrOfDefaultGlasses() // eventueel later aanpassen
        {
            return (double)ResevoirVolume * (double)this.AlcoholPerc / 100 / 12;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
