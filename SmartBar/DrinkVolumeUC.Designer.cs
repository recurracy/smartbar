﻿namespace SmartBar
{
    partial class DrinkVolumeUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgbVolume = new System.Windows.Forms.ProgressBar();
            this.lblVolume = new System.Windows.Forms.Label();
            this.pcbDrink = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDrink)).BeginInit();
            this.SuspendLayout();
            // 
            // pgbVolume
            // 
            this.pgbVolume.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgbVolume.Location = new System.Drawing.Point(3, 153);
            this.pgbVolume.Name = "pgbVolume";
            this.pgbVolume.Size = new System.Drawing.Size(156, 23);
            this.pgbVolume.TabIndex = 1;
            this.pgbVolume.Click += new System.EventHandler(this.pgbVolume_Click);
            // 
            // lblVolume
            // 
            this.lblVolume.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblVolume.AutoSize = true;
            this.lblVolume.Location = new System.Drawing.Point(63, 179);
            this.lblVolume.Name = "lblVolume";
            this.lblVolume.Size = new System.Drawing.Size(35, 13);
            this.lblVolume.TabIndex = 2;
            this.lblVolume.Text = "label1";
            this.lblVolume.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcbDrink
            // 
            this.pcbDrink.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pcbDrink.Location = new System.Drawing.Point(3, 3);
            this.pcbDrink.Name = "pcbDrink";
            this.pcbDrink.Size = new System.Drawing.Size(156, 144);
            this.pcbDrink.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbDrink.TabIndex = 0;
            this.pcbDrink.TabStop = false;
            // 
            // DrinkVolumeUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblVolume);
            this.Controls.Add(this.pgbVolume);
            this.Controls.Add(this.pcbDrink);
            this.Name = "DrinkVolumeUC";
            this.Size = new System.Drawing.Size(162, 194);
            ((System.ComponentModel.ISupportInitialize)(this.pcbDrink)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbDrink;
        private System.Windows.Forms.ProgressBar pgbVolume;
        private System.Windows.Forms.Label lblVolume;
    }
}
