﻿namespace SmartBar
{
    partial class CustomCocktailUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbServings = new System.Windows.Forms.TrackBar();
            this.lbName = new System.Windows.Forms.Label();
            this.lbAlcoholPerc = new System.Windows.Forms.Label();
            this.lbStandardGlasses = new System.Windows.Forms.Label();
            this.pbDrink = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tbServings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrink)).BeginInit();
            this.SuspendLayout();
            // 
            // tbVolume
            // 
            this.tbServings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbServings.LargeChange = 1;
            this.tbServings.Location = new System.Drawing.Point(3, 3);
            this.tbServings.Maximum = 3;
            this.tbServings.Name = "tbVolume";
            this.tbServings.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbServings.Size = new System.Drawing.Size(45, 305);
            this.tbServings.TabIndex = 0;
            this.tbServings.Scroll += new System.EventHandler(this.tbVolume_Scroll);
            // 
            // lbName
            // 
            this.lbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.Location = new System.Drawing.Point(54, 262);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(41, 13);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "label1";
            // 
            // lbAlcoholPerc
            // 
            this.lbAlcoholPerc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbAlcoholPerc.AutoSize = true;
            this.lbAlcoholPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAlcoholPerc.Location = new System.Drawing.Point(54, 275);
            this.lbAlcoholPerc.Name = "lbAlcoholPerc";
            this.lbAlcoholPerc.Size = new System.Drawing.Size(35, 13);
            this.lbAlcoholPerc.TabIndex = 3;
            this.lbAlcoholPerc.Text = "label1";
            // 
            // lbStandardGlasses
            // 
            this.lbStandardGlasses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbStandardGlasses.AutoSize = true;
            this.lbStandardGlasses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStandardGlasses.Location = new System.Drawing.Point(54, 288);
            this.lbStandardGlasses.Name = "lbStandardGlasses";
            this.lbStandardGlasses.Size = new System.Drawing.Size(35, 13);
            this.lbStandardGlasses.TabIndex = 4;
            this.lbStandardGlasses.Text = "label1";
            // 
            // pbDrink
            // 
            this.pbDrink.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbDrink.Location = new System.Drawing.Point(54, 3);
            this.pbDrink.Name = "pbDrink";
            this.pbDrink.Size = new System.Drawing.Size(158, 256);
            this.pbDrink.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDrink.TabIndex = 1;
            this.pbDrink.TabStop = false;
            // 
            // CustomCocktailUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbStandardGlasses);
            this.Controls.Add(this.lbAlcoholPerc);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.pbDrink);
            this.Controls.Add(this.tbServings);
            this.Name = "CustomCocktailUC";
            this.Size = new System.Drawing.Size(215, 311);
            ((System.ComponentModel.ISupportInitialize)(this.tbServings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDrink)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar tbServings;
        private System.Windows.Forms.PictureBox pbDrink;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbAlcoholPerc;
        private System.Windows.Forms.Label lbStandardGlasses;
    }
}
