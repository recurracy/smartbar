﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartBar
{
    public partial class CocktailUC : UserControl
    {
        public event EventHandler Clicked;

        private Cocktail cocktail;

        public CocktailUC()
        {
            InitializeComponent();
        }

        public void SetCocktail(Cocktail cocktail, int nrOfOrders)
        {
            this.cocktail = cocktail;
            this.pibCocktailMixDrankUC.Image = cocktail.Image.Image;
            string ingredients = "";
            for (int i = 0; i < cocktail.Ingredients.Count; i++)
            {
                KeyValuePair<Drink, int> kvp = cocktail.Ingredients.ElementAt(i);
                string a = string.Format("{0}x {1}{2}", kvp.Value, kvp.Key.Name, i == cocktail.Ingredients.Count - 1 ? "" : ", ");
                ingredients += a;
            }
            this.lblIngredients.Text = ingredients;
            this.lblName.Text = cocktail.Name;
            this.lblDefaultGlasses.Text = string.Format("Standaardglazen: {0}", Math.Round(cocktail.GetStandardGlasses(), 1));
            this.lblAlcoholPerc.Text = "Alcoholpercentage: " + Math.Round(cocktail.GetAlcoholPerc(), 1).ToString();
            this.lblOrderCount.Text = String.Format("{0} keer besteld", nrOfOrders);
        }

        private void btnCocktailMixDrankKiesUC_Click(object sender, EventArgs e)
        {
            Bar.CurrentBar.BeginPourCocktail(this.cocktail);
            if (Clicked != null)
                Clicked(this, EventArgs.Empty);
        }
    }
}
