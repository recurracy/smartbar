﻿namespace SmartBar
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPresetDrank = new System.Windows.Forms.Button();
            this.btnCustomDrank = new System.Windows.Forms.Button();
            this.lblBarStatus = new System.Windows.Forms.Label();
            this.pnlVolumeUCs = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btnPresetDrank
            // 
            this.btnPresetDrank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPresetDrank.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPresetDrank.Font = new System.Drawing.Font("Script MT Bold", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPresetDrank.ForeColor = System.Drawing.Color.White;
            this.btnPresetDrank.Location = new System.Drawing.Point(23, 251);
            this.btnPresetDrank.Name = "btnPresetDrank";
            this.btnPresetDrank.Size = new System.Drawing.Size(376, 248);
            this.btnPresetDrank.TabIndex = 4;
            this.btnPresetDrank.Text = "Cocktails en mixdranken";
            this.btnPresetDrank.UseVisualStyleBackColor = false;
            this.btnPresetDrank.Click += new System.EventHandler(this.btnPresetDrank_Click);
            // 
            // btnCustomDrank
            // 
            this.btnCustomDrank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCustomDrank.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomDrank.Font = new System.Drawing.Font("Script MT Bold", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomDrank.ForeColor = System.Drawing.Color.White;
            this.btnCustomDrank.Location = new System.Drawing.Point(405, 251);
            this.btnCustomDrank.Name = "btnCustomDrank";
            this.btnCustomDrank.Size = new System.Drawing.Size(376, 248);
            this.btnCustomDrank.TabIndex = 5;
            this.btnCustomDrank.Text = "Custom drank";
            this.btnCustomDrank.UseVisualStyleBackColor = false;
            this.btnCustomDrank.Click += new System.EventHandler(this.btnCustomDrank_Click);
            // 
            // lblBarStatus
            // 
            this.lblBarStatus.AutoSize = true;
            this.lblBarStatus.Location = new System.Drawing.Point(12, 9);
            this.lblBarStatus.Name = "lblBarStatus";
            this.lblBarStatus.Size = new System.Drawing.Size(35, 13);
            this.lblBarStatus.TabIndex = 10;
            this.lblBarStatus.Text = "label1";
            // 
            // pnlVolumeUCs
            // 
            this.pnlVolumeUCs.AutoScroll = true;
            this.pnlVolumeUCs.Location = new System.Drawing.Point(23, 25);
            this.pnlVolumeUCs.Name = "pnlVolumeUCs";
            this.pnlVolumeUCs.Size = new System.Drawing.Size(758, 220);
            this.pnlVolumeUCs.TabIndex = 11;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(804, 511);
            this.Controls.Add(this.pnlVolumeUCs);
            this.Controls.Add(this.lblBarStatus);
            this.Controls.Add(this.btnCustomDrank);
            this.Controls.Add(this.btnPresetDrank);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Smart Bar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPresetDrank;
        private System.Windows.Forms.Button btnCustomDrank;
        private System.Windows.Forms.Label lblBarStatus;
        private System.Windows.Forms.Panel pnlVolumeUCs;
    }
}

