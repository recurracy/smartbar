﻿namespace SmartBar
{
    partial class CocktailUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblIngredients = new System.Windows.Forms.Label();
            this.lblDefaultGlasses = new System.Windows.Forms.Label();
            this.btnCocktailMixDrankKiesUC = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblAlcoholPerc = new System.Windows.Forms.Label();
            this.pibCocktailMixDrankUC = new System.Windows.Forms.PictureBox();
            this.lblOrderCount = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibCocktailMixDrankUC)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe Marker", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(6, 23);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 14);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Naam";
            // 
            // lblIngredients
            // 
            this.lblIngredients.AutoSize = true;
            this.lblIngredients.Font = new System.Drawing.Font("Segoe Marker", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngredients.Location = new System.Drawing.Point(6, 45);
            this.lblIngredients.Name = "lblIngredients";
            this.lblIngredients.Size = new System.Drawing.Size(65, 14);
            this.lblIngredients.TabIndex = 2;
            this.lblIngredients.Text = "Ingrediënten";
            // 
            // lblDefaultGlasses
            // 
            this.lblDefaultGlasses.AutoSize = true;
            this.lblDefaultGlasses.Font = new System.Drawing.Font("Segoe Marker", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefaultGlasses.Location = new System.Drawing.Point(6, 93);
            this.lblDefaultGlasses.Name = "lblDefaultGlasses";
            this.lblDefaultGlasses.Size = new System.Drawing.Size(81, 14);
            this.lblDefaultGlasses.TabIndex = 3;
            this.lblDefaultGlasses.Text = "Standaardglazen";
            // 
            // btnCocktailMixDrankKiesUC
            // 
            this.btnCocktailMixDrankKiesUC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCocktailMixDrankKiesUC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCocktailMixDrankKiesUC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCocktailMixDrankKiesUC.Font = new System.Drawing.Font("Script MT Bold", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCocktailMixDrankKiesUC.ForeColor = System.Drawing.Color.White;
            this.btnCocktailMixDrankKiesUC.Location = new System.Drawing.Point(380, 10);
            this.btnCocktailMixDrankKiesUC.Name = "btnCocktailMixDrankKiesUC";
            this.btnCocktailMixDrankKiesUC.Size = new System.Drawing.Size(166, 145);
            this.btnCocktailMixDrankKiesUC.TabIndex = 4;
            this.btnCocktailMixDrankKiesUC.Text = "Kies";
            this.btnCocktailMixDrankKiesUC.UseVisualStyleBackColor = false;
            this.btnCocktailMixDrankKiesUC.Click += new System.EventHandler(this.btnCocktailMixDrankKiesUC_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.lblOrderCount);
            this.groupBox1.Controls.Add(this.lblAlcoholPerc);
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Controls.Add(this.lblIngredients);
            this.groupBox1.Controls.Add(this.lblDefaultGlasses);
            this.groupBox1.Font = new System.Drawing.Font("Script MT Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(164, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(210, 145);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cocktail";
            // 
            // lblAlcoholPerc
            // 
            this.lblAlcoholPerc.AutoSize = true;
            this.lblAlcoholPerc.Font = new System.Drawing.Font("Segoe Marker", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlcoholPerc.Location = new System.Drawing.Point(6, 69);
            this.lblAlcoholPerc.Name = "lblAlcoholPerc";
            this.lblAlcoholPerc.Size = new System.Drawing.Size(58, 14);
            this.lblAlcoholPerc.TabIndex = 4;
            this.lblAlcoholPerc.Text = "Alcoholperc";
            // 
            // pibCocktailMixDrankUC
            // 
            this.pibCocktailMixDrankUC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pibCocktailMixDrankUC.Location = new System.Drawing.Point(10, 10);
            this.pibCocktailMixDrankUC.Name = "pibCocktailMixDrankUC";
            this.pibCocktailMixDrankUC.Size = new System.Drawing.Size(148, 145);
            this.pibCocktailMixDrankUC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pibCocktailMixDrankUC.TabIndex = 0;
            this.pibCocktailMixDrankUC.TabStop = false;
            // 
            // lblOrderCount
            // 
            this.lblOrderCount.AutoSize = true;
            this.lblOrderCount.Font = new System.Drawing.Font("Segoe Marker", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderCount.Location = new System.Drawing.Point(6, 117);
            this.lblOrderCount.Name = "lblOrderCount";
            this.lblOrderCount.Size = new System.Drawing.Size(77, 14);
            this.lblOrderCount.TabIndex = 5;
            this.lblOrderCount.Text = "Aantal x besteld";
            // 
            // CocktailUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCocktailMixDrankKiesUC);
            this.Controls.Add(this.pibCocktailMixDrankUC);
            this.Name = "CocktailUC";
            this.Size = new System.Drawing.Size(557, 158);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibCocktailMixDrankUC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pibCocktailMixDrankUC;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblIngredients;
        private System.Windows.Forms.Label lblDefaultGlasses;
        private System.Windows.Forms.Button btnCocktailMixDrankKiesUC;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblAlcoholPerc;
        private System.Windows.Forms.Label lblOrderCount;
    }
}
