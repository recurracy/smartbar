﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SmartBar
{
    public class Cocktail
    {
        /// <summary>
        /// The maximum servings per glass that is allowed.
        /// </summary>
        public const int MaxServings = 5;

        /// <summary>
        /// The name of this cocktail.
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// The volume of this cocktail in ml.
        /// </summary>
        public int Volume { get; private set; }
        /// <summary>
        /// The description of this cocktail.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The image of this cocktail.
        /// </summary>
        public ImageEx Image { get; set; }
        /// <summary>
        /// The ingredients of this cocktail.
        /// The key of the dictionary is a drink.
        /// The value of the dictionary determines the amount of servings of that drink.
        /// </summary>
        public Dictionary<Drink, int> Ingredients { get; private set; }

        public Cocktail(string name)
        {
            this.Name = name;
            this.Ingredients = new Dictionary<Drink, int>();
        }

        /// <summary>
        /// Gets the volume of this drink in ml
        /// </summary>
        /// <returns></returns>
        public int GetVolume()
        {
            int sum = 0;
            foreach (KeyValuePair<Drink, int> kvp in this.Ingredients)
            {
                sum += kvp.Value * Drink.ResevoirVolume;
            }
            return sum;
        }

        public double GetStandardGlasses()
        {
            double sum = 0;
            int servings = 0;
            foreach(KeyValuePair<Drink, int> kvp in this.Ingredients)
            {
                sum += kvp.Key.GetNrOfDefaultGlasses() * kvp.Value;
                servings += kvp.Value;
            }
            return sum / (double)servings;
        }

        public bool CanPour()
        {
            foreach (KeyValuePair<Drink, int> kvp in this.Ingredients)
            {
                if (!kvp.Key.CanPour(kvp.Value))
                    return false;
            }
            return true;
        }

        public double GetAlcoholPerc()
        {
            double alcoholPerc = 0;
            int servings = 0;

            for (int i = 0; i < this.Ingredients.Count; i++)
            {
                KeyValuePair<Drink, int> kvp = this.Ingredients.ElementAt(i);
                int total = kvp.Key.AlcoholPerc * kvp.Value;
                alcoholPerc += total;
                servings += kvp.Value;
            }

            return alcoholPerc / servings;
        }
    }
}
