﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SmartBar
{
    public partial class CustomCocktailUC : UserControl
    {
        public event EventHandler VolumeSliderScroll;

        public int Servings
        {
            get { return this._servings; }
            set
            {
                if (value > tbServings.Maximum)
                    throw new InvalidOperationException("Value is higher than the maximum value of the slider.");
                _servings = value;
                tbServings.Value = value;
            }
        }
        public Drink Drink { get; private set; }

        private int _servings;

        public CustomCocktailUC()
        {
            InitializeComponent();
        }

        public void SetDrink(Drink drink)
        {
            int fills = drink.CurrentVolume / SmartBar.Drink.ResevoirVolume;
            this.tbServings.Maximum = fills >= Cocktail.MaxServings ? Cocktail.MaxServings : Cocktail.MaxServings - fills;

            this.Drink = drink;
            this.pbDrink.Image = drink.Image.Image;
            this.lbAlcoholPerc.Text = drink.AlcoholPerc.ToString() + "% alcohol";
            this.lbName.Text = drink.Name;
            this.lbStandardGlasses.Text = String.Format("Standaardglazen: {0}", Math.Round(drink.GetNrOfDefaultGlasses(),1));
        }

        private void tbVolume_Scroll(object sender, EventArgs e)
        {
            this._servings = tbServings.Value;
            if (VolumeSliderScroll != null)
                VolumeSliderScroll(this, EventArgs.Empty);
        }
    }
}
