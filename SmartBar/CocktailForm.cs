﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartBar
{
    public partial class CocktailForm : Form
    {
        public CocktailForm()
        {
            InitializeComponent();
            this.Shown += CocktailForm_Shown;
        }

        void CocktailForm_Shown(object sender, EventArgs e)
        {
            int x = 0;
            int y = 0;
            foreach (Drink drink in Bar.CurrentBar.AvailableDrinks.Where(d => d.CurrentVolume >= Drink.ResevoirVolume))
            {
                CustomCocktailUC uc = new CustomCocktailUC();
                uc.SetDrink(drink);
                uc.Location = new Point(x, y);
                uc.VolumeSliderScroll += uc_VolumeSliderScroll;
                x += uc.Width;
                this.pnlDrinks.Controls.Add(uc);
            }
        }

        void uc_VolumeSliderScroll(object sender, EventArgs e)
        {
            IEnumerable<CustomCocktailUC> cocktailUcs = this.pnlDrinks.Controls.OfType<CustomCocktailUC>();
            int sum = cocktailUcs.Sum(c => c.Servings);

            if (sum > Cocktail.MaxServings)
            {
                int diff = sum - Cocktail.MaxServings;
                int maxValue = cocktailUcs.Max(t => t.Servings);
                int minValue = cocktailUcs.Where(t => t.Servings != 0).Min(t => t.Servings);
                CustomCocktailUC uc = cocktailUcs.FirstOrDefault(t => t != sender && t.Servings > minValue && t.Servings <= maxValue);
                if (uc == null)
                    uc = cocktailUcs.FirstOrDefault(t => t != sender && t.Servings >= minValue && t.Servings <= maxValue);
                uc.Servings -= diff;
            }
        }

        void trackbarValueChanged(object sender, EventArgs e)
        {
            int sum = 0;
            IEnumerable<TrackBar> trackBars = this.Controls.OfType<TrackBar>();
            foreach (TrackBar tb in trackBars)
            {
                sum += tb.Value;
            }

            if (sum > Cocktail.MaxServings)
            {
                int diff = sum - Cocktail.MaxServings;
                int maxValue = trackBars.Max(t => t.Value);
                int minValue = trackBars.Where(t => t.Value != 0).Min(t => t.Value);
                TrackBar tb = trackBars.FirstOrDefault(t => t != sender && t.Value > minValue && t.Value <= maxValue);
                if (tb == null)
                    tb = trackBars.FirstOrDefault(t => t != sender && t.Value >= minValue && t.Value <= maxValue);
                tb.Value -= diff;
            }
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
            ConfirmCocktailForm form = new ConfirmCocktailForm();
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Cocktail custom = new Cocktail(form.CocktailName);
                custom.Image = Bar.CurrentBar.CustomDrinkImg;
                IEnumerable<CustomCocktailUC> ucs = this.pnlDrinks.Controls.OfType<CustomCocktailUC>().Where(u => u.Servings > 0);
                foreach (CustomCocktailUC uc in ucs)
                {
                    custom.Ingredients.Add(uc.Drink, uc.Servings);
                }

                Bar.CurrentBar.AddCocktail(custom);
            }
            this.Close();
        }
    }
}
