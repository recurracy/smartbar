﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Data.SQLite;
using System.IO;
using System.Drawing;
using SmartBar.Properties;

namespace SmartBar
{
    public class ImageEx
    {
        public Bitmap Image { get; protected set; }
        public string Filename { get; protected set; }

        public ImageEx(string path)
        {
            try
            {
                this.Image = new Bitmap(path);
                this.Filename = path.Substring(path.LastIndexOf('\\') + 1);
            }
            catch (ArgumentException e)
            {
                throw new FileNotFoundException("Bestand " + path + " is niet gevonden.");
            }
        }

        public static ImageEx FromFile(string path)
        {
            return new ImageEx(path);
        }
    }

    public class Bar
    {
        /// <summary>
        /// Thrown if the BarReady property changes.
        /// </summary>
        public event EventHandler ReadyStatusChanged;

        /*  SQLiteConnection con = new SQLiteConnection(@"Data Source=C:\Users\<Username>\Dropbox\A-TS2-C\Periode 2 (TS-2C)\PT\Database\smartbar.db;Version=3;");
            con.Open();
            string txt = "SELECT * FROM Preset;";
            SQLiteCommand cmd = new SQLiteCommand(txt, con);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                MessageBox.Show(Convert.ToString(reader["presetnaam"]));
            } */
        public static Bar CurrentBar = new Bar();

        /// <summary>
        /// The default Custom Drink image.
        /// </summary>
        public ImageEx CustomDrinkImg { get; private set; }
        /// <summary>
        /// The image that is used when an image is missing.
        /// </summary>
        public ImageEx MissingDrinkImg { get; private set; }
        /// <summary>
        /// Determines if the bar is ready or busy.
        /// </summary>
        public bool BarReady { get; set; }
        /// <summary>
        /// The list of available cocktails in the system.
        /// </summary>
        public List<Cocktail> AvailableCocktails = new List<Cocktail>();
        /// <summary>
        /// The list of available drinks in the system.
        /// </summary>
        public List<Drink> AvailableDrinks = new List<Drink>();
        /// <summary>
        /// The absolute path to the database.
        /// </summary>
        public string DatabasePath { get; private set; }
        /// <summary>
        /// The relative path to the application settings.
        /// </summary>
        public string SettingsPath { get; private set; }
        /// <summary>
        /// The relative path to the images folder.
        /// </summary>
        public string ImagesPath { get; private set; }
        /// <summary>
        /// The COM Port that is to be used for serial communication.
        /// </summary>
        public string COMPort { get; private set; }

        /// <summary>
        /// is used for connection with the serial port
        /// </summary>
        private SerialPort port = new SerialPort();
        /// <summary>
        /// is used for communication with the database
        /// </summary>
        private SQLiteConnection sqlConnection;

        /// <summary>
        /// Returns a random bitmap from the Images folder.
        /// </summary>
        /// <returns>Bitmap</returns>
        Bitmap randomBitmap()
        {
            Bitmap img = null;
            string[] files = Directory.GetFiles(ImagesPath);
            Random rand = new Random();
            while (img == null)
            {
                int i = rand.Next(0, files.Count());
                try
                {
                    Bitmap temp = new Bitmap(files[i]);
                    img = temp;
                }
                catch
                {
                    continue;
                }
            }
            return img;
        }

        /// <summary>
        /// Initializes the Serial Port.
        /// </summary>
        public void InitializePort()
        {
            if (port.IsOpen)
            {
                port.RtsEnable = false;
                port.DtrEnable = false;
                port.Close();
            }

            port.PortName = COMPort;
            port.BaudRate = 9600;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.RtsEnable = true;
            port.DataReceived += port_DataReceived;
            try
            {
                if (!port.IsOpen)
                    port.Open();
                BarReady = true;
            }
            catch (Exception e)
            {
                
            }
        }

        /// <summary>
        /// Eventhandler that handles Serial Port DataReceived event.
        /// </summary>
        /// <param name="sender">The sender of this event</param>
        /// <param name="e">The Event Arguments of this event</param>
        void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string data = port.ReadLine();
            data = data.Replace("#", "");
            data = data.Replace("%", "");
            data = data.Replace("\r", "");
            data = data.Replace("\n", "");

            if (data == "0")
            {
                BarReady = false;
                if (this.ReadyStatusChanged != null)
                    ReadyStatusChanged(this, EventArgs.Empty);
            }
            if (data == "1")
            {
                BarReady = true;
                if (this.ReadyStatusChanged != null)
                    ReadyStatusChanged(this, EventArgs.Empty);
            }
        }

        #region DATABASE

        /// <summary>
        /// Initializes connection to SQLite.
        /// </summary>
        public void InitializeDBConnection()
        {
            this.sqlConnection = new SQLiteConnection(@"Data Source=" + DatabasePath + ";Version=3;");
            sqlConnection.Open();
        }

        ///<summary>
        /// Saves a new Cocktail in the system.
        ///</summary>
        public void AddCocktail(Cocktail cocktail)
        {
            this.AvailableCocktails.Add(cocktail);
            SQLiteCommand cocktailCmd = new SQLiteCommand("INSERT INTO Cocktail(name, imgpath) VALUES(?,?)", sqlConnection);
            cocktailCmd.Parameters.Add(new SQLiteParameter("name", cocktail.Name));
            cocktailCmd.Parameters.Add(new SQLiteParameter("imgpath", cocktail.Image.Filename));

            foreach (KeyValuePair<Drink, int> kvp in cocktail.Ingredients)
            {
                SQLiteCommand ingredientCmd = new SQLiteCommand("INSERT INTO Ingredientline(cocktailnr, drinknr, servings) VALUES(?,?,?)", sqlConnection);
                ingredientCmd.Parameters.Add(new SQLiteParameter("cocktailnr", this.AvailableCocktails.IndexOf(cocktail) + 1));
                ingredientCmd.Parameters.Add(new SQLiteParameter("drinknr", this.AvailableDrinks.IndexOf(kvp.Key) + 1));
                ingredientCmd.Parameters.Add(new SQLiteParameter("servings", kvp.Value));
                try
                {
                    ingredientCmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }

            try
            {
                cocktailCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        ///<summary>
        /// Adds a new row to the Drink table in the DB.
        ///</summary>
        public void AddDrink()
        {

        }
        ///<summary>
        /// Removes a row from the Cocktail table in the DB.
        ///</summary>
        public void RemoveCocktail()
        {

        }
        ///<summary>
        /// Removes a row from the Drink table in the DB.
        ///</summary>
        public void RemoveDrink()
        {

        }

        ///<summary>
        /// Retrieve order data from the DB. Is ordered by count.
        ///</summary>
        public Dictionary<Cocktail, int> GetCocktailOrdersDesc() // weet je een betere naam? verander maar! :)
        {
            SQLiteCommand orderCmd = new SQLiteCommand("SELECT COUNT(*) AS ordercount, cocktailnr FROM cocktailorder\n" +
                "GROUP BY cocktailnr ORDER BY ordercount DESC", sqlConnection);
            SQLiteDataReader reader = orderCmd.ExecuteReader();
            Dictionary<Cocktail, int> cocktails = new Dictionary<Cocktail, int>();
            while (reader.Read())
            {
                int count = Convert.ToInt32(reader["ordercount"]);
                int index = Convert.ToInt32(reader["cocktailnr"]) - 1;
                Cocktail c = AvailableCocktails[index];
                cocktails.Add(c, count);
            }
            foreach (Cocktail coc in AvailableCocktails)
            {
                if (!cocktails.ContainsKey(coc))
                    cocktails.Add(coc, 0);
            }

            return cocktails;
        }

        /// <summary>
        /// Updates the current volume of a drink in the DB.
        /// </summary>
        /// <param name="drink">The drink to be updated.</param>
        public void UpdateDrinkVolume(Drink drink)
        {
            SQLiteCommand updateCmd = new SQLiteCommand("UPDATE DRINK SET currentvolume = @curvol WHERE drinknr = @drinknr;", sqlConnection);
            updateCmd.Parameters.Add(new SQLiteParameter("@curvol", drink.CurrentVolume));
            updateCmd.Parameters.Add(new SQLiteParameter("@drinknr", AvailableDrinks.IndexOf(drink) + 1));
            try
            {
                updateCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Inserts a new row into the Order table in the DB.
        /// </summary>
        /// <param name="cocktail">The cocktail that has been ordered that must be added to the DB.</param>
        public void AddOrder(Cocktail cocktail)
        {
            SQLiteCommand insertCmd = new SQLiteCommand("INSERT INTO Cocktailorder(ordernr, cocktailnr) VALUES(?,?)", sqlConnection);
            int index = AvailableCocktails.IndexOf(cocktail) + 1;
            if (index == -1)
                throw new ArgumentException("Deze cocktail staat niet in de lijst van beschikbare cocktails.");
            insertCmd.Parameters.Add(new SQLiteParameter("ordernr", null));
            insertCmd.Parameters.Add(new SQLiteParameter("cocktailnr", index));
            try
            {
                insertCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retrieves Drink data from the DB.
        /// </summary>
        public void LoadDataDrinks()
        {
            string txt = "SELECT * FROM Drink ORDER BY drinknr;";
            SQLiteCommand cmd = new SQLiteCommand(txt, sqlConnection);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string naam = Convert.ToString(reader["name"]);
                int maxvol = Convert.ToInt32(reader["maxvolume"]);
                int alcper = Convert.ToInt32(reader["alcoholperc"]);
                int nr = Convert.ToInt32(reader["drinknr"]);
                int curVol = Convert.ToInt32(reader["currentvolume"]);
                object img = reader["imgpath"];

                Drink d = new Drink(naam, maxvol, alcper);
                d.CurrentVolume = curVol;
                try
                {
                    if (img.GetType() != typeof(DBNull))
                        d.Image = new ImageEx(ImagesPath + "\\" + (string)img);
                    else
                        d.Image = new ImageEx(ImagesPath + "\\missingimg.png");
                    //d.Image = new Bitmap(ImagesPath + "\\" + (string)img);
                }
                catch (Exception e)
                {
                    d.Image = new ImageEx(ImagesPath + "\\missingimg.png");
                }
                AvailableDrinks.Insert(nr - 1, d);
            }
        }

        /// <summary>
        /// Retrieves Cocktail data from the DB. 
        /// </summary>
        public void LoadDataCocktails()
        {
            string txt = "SELECT * FROM Cocktail ORDER BY cocktailnr;";
            SQLiteCommand cmd = new SQLiteCommand(txt, sqlConnection);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string naam = Convert.ToString(reader["name"]);
                int nr = Convert.ToInt32(reader["cocktailnr"]);
                object img = reader["imgpath"];
                Cocktail c = new Cocktail(naam);

                try
                {
                    if (img.GetType() != typeof(DBNull))
                        c.Image = new ImageEx(ImagesPath + "\\" + (string)img);
                    else
                        c.Image = new ImageEx(ImagesPath + "\\missingimg.png");
                    //c.Image = new Bitmap(ImagesPath + "\\" + (string)img);
                }
                catch (Exception e)
                {
                    c.Image = new ImageEx(ImagesPath + "\\missingimg.png");
                    //c.Image = Resources.missingimg;
                }

                AvailableCocktails.Insert(nr - 1, c);
            }
        }

        /// <summary>
        /// Retrieves Cocktail Ingredients data from the DB.
        /// </summary>
        public void LoadCocktailIngredients()
        {
            string txt = "SELECT * FROM Ingredientline";
            SQLiteCommand cmd = new SQLiteCommand(txt, sqlConnection);
            SQLiteDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int cocktailId = Convert.ToInt32(reader["cocktailnr"]);
                int drinkId = Convert.ToInt32(reader["drinknr"]);
                int servings = Convert.ToInt32(reader["servings"]);
                Cocktail c = AvailableCocktails[cocktailId - 1];
                Drink d = AvailableDrinks[drinkId - 1];
                c.Ingredients.Add(d, servings);
            }
        }

        #endregion

        ///<summary>
        /// Send pour commands to the arduino.
        ///</summary>
        public bool BeginPourCocktail(Cocktail cocktail)
        {
            if (!BarReady)
                return false;

            AddOrder(cocktail);

            //waarschijnlijk argument is 1 van de 4 steppenmotoren die bestuurd moet worden.
            string msg = "#";
            foreach (KeyValuePair<Drink, int> kvp in cocktail.Ingredients)
            {
                for (int i = 0; i < kvp.Value; i++)
                {
                    msg += AvailableDrinks.IndexOf(kvp.Key);
                }
            }
            msg += ";";
            port.Write(msg);

            foreach (KeyValuePair<Drink, int> kvp in cocktail.Ingredients)
            {
                int volume = Drink.ResevoirVolume * kvp.Value;
                kvp.Key.CurrentVolume -= volume;
                UpdateDrinkVolume(kvp.Key);
            }

            return true;
        }

        /// <summary>
        /// Loads local settings.
        /// </summary>
        public void LoadSettings()
        {
            SettingsPath = System.Windows.Forms.Application.StartupPath + "\\smartbarsettings.txt";
            ImagesPath = System.Windows.Forms.Application.StartupPath + "\\imgs";
            if (!Directory.Exists(ImagesPath))
                Directory.CreateDirectory(ImagesPath);

            Resources.customdrank.Save(ImagesPath + "\\customdrank.png");
            Resources.missingimg.Save(ImagesPath + "\\missingimg.png");
            this.CustomDrinkImg = ImageEx.FromFile(ImagesPath + "\\customdrank.png");
            this.MissingDrinkImg = ImageEx.FromFile(ImagesPath + "\\missingimg.png");

            if (!File.Exists(SettingsPath))
            {
                using (StreamWriter writer = new StreamWriter(SettingsPath))
                {
                    writer.WriteLine("dbpath=<VERANDER>");
                    writer.WriteLine("comport=<VERANDER>");
                }
            }
            using (StreamReader reader = new StreamReader(SettingsPath))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains("dbpath"))
                    {
                        DatabasePath = line.Replace("dbpath=", "");
                    }
                    if (line.Contains("comport"))
                    {
                        COMPort = line.Replace("comport=", "");
                    }
                }
            }
        }
    }
}
